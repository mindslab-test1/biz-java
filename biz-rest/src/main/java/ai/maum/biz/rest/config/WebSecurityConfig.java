package ai.maum.biz.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User.withDefaultPasswordEncoder().username("admin").password("1234").roles("USER").build());
        return manager;
    }

    @Configuration
    static class Security extends WebSecurityConfigurerAdapter {
        private SecurityProperties securityProperties;

        @Autowired
        public void setSecurityProperties(SecurityProperties securityProperties) {
            this.securityProperties = securityProperties;
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        private AuthenticationEntryPoint authenticationEntryPoint() {
            return new BasicAuthenticationEntryPoint();
        }

        private BasicAuthenticationFilter basicAuthenticationFilter(
                AuthenticationManager authenticationManager) {
            return new BasicAuthenticationFilter(authenticationManager, authenticationEntryPoint());
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            /*
            if (securityProperties.isEnableCsrf()) {
                http.csrf();
            } else {
                http.csrf().disable();
            }
​*/
            /*
            http
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/post/**").permitAll()
                    .antMatchers(HttpMethod.POST, "/post/*").hasRole("USER")
                    .antMatchers(HttpMethod.DELETE, "/post/*").hasRole("USER")
                    .anyRequest().authenticated()
                    .and()
                    .addFilterAt(basicAuthenticationFilter(authenticationManagerBean()), BasicAuthenticationFilter.class);
            */
            http.csrf().disable();

            http.authorizeRequests()
                    .antMatchers("/rest/**").hasRole("ADMIN")
                    .antMatchers("/**").permitAll();

        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/css/**\", \"/script/**\", \"image/**\", \"/fonts/**\", \"lib/**");
        }
    }
}
