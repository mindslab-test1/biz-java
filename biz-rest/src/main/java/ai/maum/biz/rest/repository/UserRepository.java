package ai.maum.biz.rest.repository;

import ai.maum.biz.rest.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
