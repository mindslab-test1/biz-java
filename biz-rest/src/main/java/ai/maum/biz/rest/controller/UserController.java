package ai.maum.biz.rest.controller;

import ai.maum.biz.rest.entity.User;
import ai.maum.biz.rest.entity.UserRole;
import ai.maum.biz.rest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PostMapping("")
    public String create(User user) {
        UserRole role = new UserRole();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        role.setRoleName("BASIC");
        user.setRoles(Arrays.asList(role));
        userRepository.save(user);

        return "redirect:/";
    }
}
