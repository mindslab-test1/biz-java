
1. gradle 빌드에서 lombok 설정

    - @Getter, @Setter 를 이용한 lombok 컴파일 옵션 사용 시 
    - idea IntelliJ 기준 다음과 같은 설정 필요

    - `File > Settings > Build, Execution, Deployment > Compiler > Annotation Processors`
    - 위 메뉴로 들어가면 좌측에  Enable annotation processing 체크를 필시 해주어야 한다.


2. Gradle 빌드 옵션
    - `File > Project Structure > ProjectSettings > Project`
    - 위 메뉴에서 Project compiler output 디렉토리 주소 끝부분을 out > build 로 수정

3. angular build 시
    - `ERROR in **** from Terser TypeError: Cannot read property 'minify' of undefined `
    - 위 에러 발생시 angular terser 모듈이 설치 되지 않았거나 낮은 버전이라서 생기는 문제 아래 명령으로 모듈 설치
    - `npm install --save terser`
       
